<?php

namespace App\Http\Controllers;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Illuminate\Http\Request;

class Example extends Controller
{
    public function testing() {
        $process = new Process('python ' . app_path('Python/pdfdecoder.py') . ' ' . app_path('Python/demo.pdf'));
        $process->run();

        if (!$process->isSuccessful()) {
            throw new ProcessFailedException($process);
        }
        echo $process->getOutput();
    }
}
